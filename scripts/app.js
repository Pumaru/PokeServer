const express = require("express");
const hostname = "0.0.0.0";
const port = 8002;
/*-*/
const app = express();
/*-*/
// Para imprimir una foto de manera estática.
app.use('/static', express.static('public'));
// Para levantar un servidor nodemon NPM.
app.listen(port, hostname, function() {
    console.log(`Server running at http://${hostname}:${port}/`);
});
/*-*/
/////////////////////////////////////////////////////////////////
//                                                             //
//                      ESPACIO DE RELLENO                     //
//                                                             //
/////////////////////////////////////////////////////////////////
/*-*/
// Array para imprimir los bichos en formato JSON.
const PokeDex = [
    {
        'results': [
            {
                'name': "bulbasaur",
                'url': "https://pokeapi.co/api/v2/pokemon/1/",
            },
            {
                'name': "ivysaur",
                'url': "https://pokeapi.co/api/v2/pokemon/2/",
            },
            {
                'name': "venusaur",
                'url': "https://pokeapi.co/api/v2/pokemon/3/",
            },
            {
                'name': "charmander",
                'url': "https://pokeapi.co/api/v2/pokemon/4/",
            },
            {
                'name': "charmeleon",
                'url': "https://pokeapi.co/api/v2/pokemon/5/",
            },
            {
                'name': "charizard",
                'url': "https://pokeapi.co/api/v2/pokemon/6/",
            },
            {
                'name': "squirtle",
                'url': "https://pokeapi.co/api/v2/pokemon/7/",
            },
            {
                'name': "wartortle",
                'url': "https://pokeapi.co/api/v2/pokemon/8/",
            },
            {
                'name': "blastoise",
                'url': "https://pokeapi.co/api/v2/pokemon/9/",
            },
            {
                'name': "caterpie",
                'url': "https://pokeapi.co/api/v2/pokemon/10/",
            },
            {
                'name': "metapod",
                'url': "https://pokeapi.co/api/v2/pokemon/11/",
            },
            {
                'name': "butterfree",
                'url': "https://pokeapi.co/api/v2/pokemon/12/",
            },
            {
                'name': "weedle",
                'url': "https://pokeapi.co/api/v2/pokemon/13/",
            },
            {
                'name': "kakuna",
                'url': "https://pokeapi.co/api/v2/pokemon/14/",
            },
            {
                'name': "beedrill",
                'url': "https://pokeapi.co/api/v2/pokemon/15/",
            },
            {
                'name': "pidgey",
                'url': "https://pokeapi.co/api/v2/pokemon/16/",
            },
            {
                'name': "pidgeotto",
                'url': "https://pokeapi.co/api/v2/pokemon/17/",
            },
            {
                'name': "pidgeot",
                'url': "https://pokeapi.co/api/v2/pokemon/18/",
            },
            {
                'name': "rattata",
                'url': "https://pokeapi.co/api/v2/pokemon/19/",
            },
            {
                'name': "raticate",
                'url': "https://pokeapi.co/api/v2/pokemon/20/",
            },
        ]
    }
];
/*-*/
// Función para coger los nombres de todos los bichos.
function PokeNombre(name) {
    for (let i = 0; i < PokeDex.length; i++){
        const PokeBD = PokeDex[i].results.find(p => p.name.replace(/"/g, '').toLowerCase() === name);
        if (PokeBD) {
            return PokeBD;
        }
    }
    return null;
};
/*-*/
// Solicitud para imprimir TODOS los bichos.
app.get('/Pokemon', (req, res) => {
        res.json(PokeDex);
});
// Solicitud para imprimir solamente el nombre y la URL de un bicho concreto.
// Se utiliza un parámetro en la URL para indicar el nombre del Pokemon.
app.get('/:name', (req, res) => {
    const name = req.params.name.toLowerCase();
    const pokemon = PokeNombre(name);
        if (pokemon) {
            res.json({
                name: pokemon.name,
                url: pokemon.url,
            });
        }
        else {
            res.status(404).send(`Pokemon ${name} no encontrado.`);
        }
});